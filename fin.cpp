#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include"opencv2/opencv.hpp"
#include <bits/stdc++.h>
using namespace std;
using namespace cv;

void LinHoughTrans(Mat src,vector<Vec4i>& lines,vector<Vec2f>& rhotheta,int rho,int theta,int threshCount, int minLength, int maxGap)
{

  int dig =(src.rows + src.cols - 2)*2;
   // sqrt(pow(src.rows,2)+pow(src.cols,2));
  //Intializing the accumlator and line points
  int** count = new int* [180];
  for(int i = 0; i < 180; i++)
   {
      count[i] = new int[2*dig];
     //memset(count[i],0,sizeof(count[i]));
   }
   for(int i = 0; i < 180; i++)
     for(int j = 0; j < 2*dig; j++)
         count[i][j] = 0;

   int*** l = new int** [180];
   for(int i = 0; i < 180; i++)
    l[i] = new int*[2*dig];
  for(int i = 0; i < 180; i++)
    for(int j = 0; j < 2*dig; j++)
     {
       l[i][j] = new int[4];
       //memset(l[i][j],0,sizeof(l[i][j]));
     }
     for(int i = 0; i < 180; i++)
       for(int j = 0; j < 2*dig; j++)
         for(int z = 0; z < 4; z++)
           l[i][j][z] = 0;

   Vec4i line;
   Vec2f rt;
   //int tOff = theta / (CV_PI/180);
 //  cout << count[0][0] << endl;
   for(int i = 0 ; i < src.rows; i++)
     for(int j = 0; j < src.cols; j++)
     {
         if(src.at<uchar>(i,j))
         {
             for(int x = 0; x < 180; x++)
             {
               int r = i * cos(x * (CV_PI/180)) + j * sin(x * (CV_PI/180)) + dig;
               if(count[x][r] == 0)
               {
                   l[x][r][0] = i;
                   l[x][r][1] = j;
                   l[x][r][2] = i;
                   l[x][r][3] = j;
               }
               else{
                     int diff = sqrt(pow(i - l[x][r][2],2)+pow(j - l[x][r][3],2));
                     if(diff <= maxGap)
                     {
                       l[x][r][2] = i;
                       l[x][r][3] = j;
                     }
                     else
                     {
                         if(count[x][r] >= threshCount
                         && sqrt(pow(l[x][r][0] - l[x][r][2],2)+pow(l[x][r][1] - l[x][r][3],2)) >= minLength)
                         {
                           line[0] = l[x][r][0];
                           line[1] = l[x][r][1];
                           line[2] = l[x][r][2];
                           line[3] = l[x][r][3];
                           rt[0] = r;
                           rt[1] = x;
                           lines.push_back(line);
                           rhotheta.push_back(rt);
                           count[x][r] = 0;
                           l[x][r][0] = i;
                           l[x][r][1] = j;
                           l[x][r][2] = i;
                           l[x][r][3] = j;
                         }
                         else{
                           count[x][r] = 0;
                           l[x][r][0] = i;
                           l[x][r][1] = j;
                           l[x][r][2] = i;
                           l[x][r][3] = j;
                         }

                     }
               }
               count[x][r]++;
             }
         }
     }

     for(int i = 0 ; i < 180; i ++)
         for(int j = 0; j < 2*dig; j++)
           if(count[i][j] >= threshCount
           && sqrt(pow(l[i][j][0]-l[i][j][2],2)+pow(l[i][j][1]-l[i][j][3],2)) >= minLength)
             {
               line[0] = l[i][j][0];
               line[1] = l[i][j][1];
               line[2] = l[i][j][2];
               line[3] = l[i][j][3];
               rt[0] = j;
               rt[1] = i;
               rhotheta.push_back(rt);
               lines.push_back(line);
             }

       for(int i = 0; i < rhotheta.size()-1; i++)
         for(int j = i+1; j < rhotheta.size(); j++)
         {
             if(abs(rhotheta[i][0] - rhotheta[j][0]) < rho && abs(rhotheta[i][1] - rhotheta[j][1]) < theta)
             {
               int len1 = sqrt(pow(lines[i][0]-lines[i][2],2) + pow(lines[i][1]-lines[i][3],2));
               int len2 = sqrt(pow(lines[j][0]-lines[j][2],2) + pow(lines[j][1]-lines[j][3],2));
               if(len1 < len2)
               {
                 lines[i][0] = lines[j][0];
                 lines[i][1] = lines[j][1];
                 lines[i][2] = lines[j][2];
                 lines[i][3] = lines[j][3];
               }
               lines.erase(lines.begin()+j,lines.begin()+j+1);
               rhotheta.erase(rhotheta.begin()+j,rhotheta.begin()+j+1);
               j--;
             }
         }
}

void RectIntersect(vector<Vec4i>& lines,vector<int> ind,vector<Vec2i>&points,int diff)
{
   Vec2i point;
   for(int i = 0; i < ind.size(); i+=2)
   {
     if(abs(lines[ind[i]][0] - lines[ind[i+1]][0]) < diff && abs(lines[ind[i]][1] - lines[ind[i+1]][1]) < diff)
             {
               lines[ind[i]][0] = (lines[ind[i]][0] + lines[ind[i+1]][0]) / 2;
               lines[ind[i+1]][0] = lines[ind[i]][0];
               lines[ind[i]][1] = (lines[ind[i]][1] + lines[ind[i+1]][1]) / 2;
               lines[ind[i+1]][1] = lines[ind[i]][1];
               point[0] = lines[ind[i]][0];
               point[1] = lines[ind[i]][1];
               points.push_back(point);
             }
             else if(abs(lines[ind[i]][0] - lines[ind[i+1]][2]) < diff && abs(lines[ind[i]][1] - lines[ind[i+1]][3]) < diff)
             {
               lines[ind[i]][0] = (lines[ind[i]][0] + lines[ind[i+1]][2]) / 2;
               lines[ind[i+1]][2] = lines[ind[i]][0];
               lines[ind[i]][1] = (lines[ind[i]][1] + lines[ind[i+1]][3]) / 2;
               lines[ind[i+1]][3] = lines[ind[i]][1];
               point[0] = lines[ind[i]][0];
               point[1] = lines[ind[i]][1];
               points.push_back(point);
               }
                 else if(abs(lines[ind[i]][2] - lines[ind[i+1]][0]) < diff && abs(lines[ind[i]][3] - lines[ind[i+1]][1]) < diff)
               {
                 lines[ind[i]][2] = (lines[ind[i]][2] + lines[ind[i+1]][0]) / 2;
                 lines[ind[i+1]][0] = lines[ind[i]][2];
                 lines[ind[i]][3] = (lines[ind[i]][3] + lines[ind[i+1]][1]) / 2;
                 lines[ind[i+1]][1] = lines[ind[i]][3];
                 point[0] = lines[ind[i]][2];
                 point[1] = lines[ind[i]][3];
                 points.push_back(point);
               }
                 else if(abs(lines[ind[i]][2] - lines[ind[i+1]][2]) < diff && abs(lines[ind[i]][3] - lines[ind[i+1]][3]) < diff)
               {
                 lines[ind[i]][2] = (lines[ind[i]][2] + lines[ind[i+1]][2]) / 2;
                 lines[ind[i+1]][2] = lines[ind[i]][2];
                 lines[ind[i]][3] = (lines[ind[i]][3] + lines[ind[i+1]][3]) / 2;
                 lines[ind[i+1]][3] = lines[ind[i]][3];
                 point[0] = lines[ind[i]][2];
                 point[1] = lines[ind[i]][3];
                 points.push_back(point);
               }
   }
  //  cout << points.size() << endl;
  //  for(int i = 0 ; i < points.size(); i++)
  //        cout << points[i] << endl;
}

void RectDetector(Mat src, vector<Vec4i>& lines,vector<Vec2f>& rhotheta,vector<Vec2i>& points,int diffDis, int diffAng)
{
  int dig =(src.rows + src.cols - 2)*2;
  vector<Vec2f> lineVectors;
  for(int i = 0; i < lines.size(); i++)
  {
    Vec2f vec;
    vec[0] = lines[i][2] - lines[i][0];
    vec[1] = lines[i][3] - lines[i][1];
    lineVectors.push_back(vec);
  }

 int countT = 0;
 vector<int> ind;
 for(int i = 0; i < lineVectors.size(); i++)
   for(int j = i+1; j < lineVectors.size(); j++)
     for(int k = j+1; k < lineVectors.size(); k++)
       for(int l = k+1; l < lineVectors.size(); l++)
       {
         float m[4];
         int countN = 0, count = 0;
         if(lineVectors[i][0] != 0)
            m[0] = atan((lineVectors[i][1] / lineVectors[i][0]))  * (180/CV_PI);
         else
            m[0] = 90;
         if(lineVectors[j][0] != 0)
            m[1] = atan((lineVectors[j][1] / lineVectors[j][0])) * (180/CV_PI);
         else
             m[1] = 90;
           if(lineVectors[k][0] != 0)
             m[2] = atan((lineVectors[k][1] / lineVectors[k][0])) * (180/CV_PI);
           else
             m[2] = 90;
           if(lineVectors[l][0] != 0)
             m[3] = atan((lineVectors[l][1] / lineVectors[l][0])) * (180/CV_PI);
           else
             m[3] = 90;

             map<int,int> indMap;
             indMap.clear();
             indMap[0] = i;
             indMap[1] = j;
             indMap[2] = k;
             indMap[3] = l;

             int i = 0;
             int p[100];
             for(int x = 0; x < 4; x++)
               for(int y = x+1; y < 4; y++)
               {
                 if(abs(m[x] - m[y]) < diffAng)
                   count++;
                 else if(abs(m[x] - m[y]) < 90 + diffAng && abs(m[x] - m[y]) > 90 - diffAng)
                 {
                   countN++;
                   p[i++] = indMap[x];
                   p[i++] = indMap[y];
                 }
               }

               if(count  == 2 && countN == 4)
               {
                  countT++;
                  for(int x = 0; x < 8; x++)
                     ind.push_back(p[x]);
                  //Do Something
               }

               //  countT = ind.size();

       }

       cout << countT << endl;
       // cout << ind.size() << endl;
       // for(int i = 0; i < ind.size(); i+=2)
       // {
       //   cout << lines[ind[i]] << endl;
       //   cout << lines[ind[i+1]] << endl;
       //
       // }

     RectIntersect(lines,ind,points,diffDis);
}

void CirHoughTrans(Mat src,vector<Vec3f>& circles,float threshratio,int minR, int maxR,int diff)
{
  int maxCount = -1;
    int rmin,rmax;
    if(minR == 0)
      rmin = 0;
    else
      rmin = minR;

      if(maxR == 0)
        rmax = 20;
      else
        rmax = maxR;

    const int rsize = rmax-rmin+1;
    const int xmax = src.rows + rmax;
    const int ymax = src.cols + rmax;

    int*** count = new int** [rsize];
    for(int i = 0 ;i < rsize;i++)
    count[i] = new int*[xmax];
    for(int i = 0 ;i < rsize;i++)
      for(int j = 0 ;j < xmax;j++)
        {
          count[i][j] = new int[ymax];
          memset(count[i][j],0,sizeof(count[i][j]));
        }
    for(int i = 0 ; i < src.rows; i ++)
      for(int j = 0; j < src.cols; j++)
          if(src.at<uchar>(i,j) != 0)
            for(int r = 0; r < rsize; r += 1)
                for(int theta = 0; theta < 360; theta++)
                {
                  int cx = i - (r+rmin) * cos(theta * (CV_PI/180));
                  int cy = j - (r+rmin) * sin(theta * (CV_PI/180));
                  if(cx >= 0 && cy >= 0 && cx < src.rows && cy < src.cols)
                  {  count[r][cx][cy]++;
                    if(count[r][cx][cy] > maxCount)
                      maxCount = count[r][cx][cy];
                  }
                }

      for(int r = 0 ;r < rsize; r++)
        for(int i = 0; i < src.rows; i++)
          for(int j = 0; j < src.cols; j++)
          {
              Vec3f circle;
              int threshold = maxCount*threshratio;
              if(count[r][i][j] > threshold)
                {
                  //cout << count[r][i][j] << endl;
                  circle[0] = r+rmin;
                  circle[1] = i;
                  circle[2] = j;
                  circles.push_back(circle);
                }

          }

        for( size_t i = 0; i < circles.size(); i++)
          for( size_t j = i+1; j < circles.size(); j++)
          {
          //  int cx, cy ,r;
            if(sqrt(pow(circles[i][1]-circles[j][1],2)+pow(circles[i][2]-circles[j][2],2)) < diff && abs(circles[i][0] - circles[j][0]) < diff)
            {
              circles[i][1] = (circles[i][1]+circles[j][1])/2;
              circles[i][2] = (circles[i][2]+circles[j][2])/2;
              circles[i][0] = (circles[i][0]+circles[j][0])/2;
              circles.erase(circles.begin()+j,circles.begin()+j+1);
              j--;
            }


          }


    return;

}

/** @function main */
int main(int argc, char** argv)
{
  Mat src, src_gray;
  int scale = 1;
  int delta = 0;
  int ddepth = CV_16S;

  /// Read the image
  src = imread( argv[1], 1 );

  if( !src.data )
    { return -1; }

  /// Convert it to gray
  cvtColor( src, src_gray, CV_BGR2GRAY );

  Mat edge,inter;

  /// Reduce the noise so we avoid false circle detection
  //GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

  //GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

  /// Generate grad_x and grad_y
 //  Mat grad_x, grad_y;
 //  Mat abs_grad_x, abs_grad_y;
 //
 //  /// Gradient X
 // //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
 // Sobel( src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
 // convertScaleAbs( grad_x, abs_grad_x );
 //
 // /// Gradient Y
 // //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
 // Sobel( src_gray, grad_y, ddepth, 1, 1, 3, scale, delta, BORDER_DEFAULT );
 // convertScaleAbs( grad_y, abs_grad_y );
 //
 // /// Total Gradient (approximate)
 // addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, src_gray );

  blur(src_gray,src_gray,Size(9,9));

//  Canny(edge, edge, 80 , 100);

  blur(src_gray, edge,Size(5,5));

  Canny(edge, edge, 80 , 100);

  // GaussianBlur(edge, inter, cv::Size(0, 0), 3);
  // addWeighted(inter, 1.5, edge, -0.5, 0, edge);

  vector<Vec3f> circles;
  vector<Vec4i> lines;
  vector<Vec2f> rhotheta;
  vector<Vec2i> points;

  CirHoughTrans(edge,circles,0.8,20,100,10);
  //
  // vector<Vec3f> circles;
  //
  // /// Apply the Hough Transform to find the circles
  // HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );

  // Draw the circles detected
  LinHoughTrans(edge, lines,rhotheta,50,5,75,75,50);
  //TODO: Implement Rectangle detector
  RectDetector(edge,lines,rhotheta,points,100,10);

  for( size_t i = 0; i < points.size(); i+=4)
  {
    cout << "Rectangle " << i/4 + 1 << " Corners: " << endl;
    cout << points[i] << " " << points[i+1] << " " << points[i+2] << " "<< points[i+3] << endl;
    float area = sqrt(pow(points[i][0]-points[i+1][0],2)+pow(points[i][1]-points[i+1][1],2)) * sqrt(pow(points[i+1][0]-points[i+2][0],2)+pow(points[i+1][1]-points[i+2][1],2));
    // if(area > 80000)
    //   cout << "It's 200 pound" << endl;
  }

  cout << lines.size() << endl;
  for( size_t i = 0; i < lines.size(); i++ )
  {
    Vec4i l = lines[i];
    line( src, Point(l[1], l[0]), Point(l[3], l[2]), Scalar(0,0,255), 3, CV_AA);
    //cout << Point(l[1], l[0]) << "" << Point(l[3], l[2]) << endl;
    //cout << rhotheta[i][0] << " " << rhotheta[i][1] << endl;
    //cout << sqrt(pow(l[1] - l[3],2)+pow(l[0] - l[2],2)) << endl;
  }

  cout << circles.size() << endl;
  for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][2]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][0]);
      cout << "Circle: " << i+1 << " " <<center << " " << radius << endl;
      if(radius > 28 && radius < 33)
        cout << "It is 1 pound!!" << endl;
      // circle center
      circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
      // circle outline
      circle( src, center, radius, Scalar(0,0,255), 3, 8, 0 );
   }

  /// Show your results
  namedWindow( "Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE );
  imshow( "Hough Circle Transform", edge);
  imshow( "Hough Circle Transform Demo", src);


  waitKey(0);
  return 0;
}

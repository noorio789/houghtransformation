#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include"opencv2/opencv.hpp"
#include <bits/stdc++.h>
using namespace std;
using namespace cv;

void CirHoughTrans(Mat src,vector<Vec3f>& circles,float threshratio,int minR, int maxR,int diff)
{
  int maxCount = -1;
    int rmin,rmax;
    if(minR == 0)
      rmin = 0;
    else
      rmin = minR;

      if(maxR == 0)
        rmax = 20;
      else
        rmax = maxR;

    const int rsize = rmax-rmin+1;
    const int xmax = src.rows + rmax;
    const int ymax = src.cols + rmax;

    int*** count = new int** [rsize];
    for(int i = 0 ;i < rsize;i++)
    count[i] = new int*[xmax];
    for(int i = 0 ;i < rsize;i++)
      for(int j = 0 ;j < xmax;j++)
        {
          count[i][j] = new int[ymax];
          memset(count[i][j],0,sizeof(count[i][j]));
        }
    for(int i = 0 ; i < src.rows; i ++)
      for(int j = 0; j < src.cols; j++)
          if(src.at<uchar>(i,j) != 0)
            for(int r = 0; r < rsize; r += 1)
                for(int theta = 0; theta < 360; theta++)
                {
                  int cx = i - (r+rmin) * cos(theta * (CV_PI/180));
                  int cy = j - (r+rmin) * sin(theta * (CV_PI/180));
                  if(cx >= 0 && cy >= 0 && cx < src.rows && cy < src.cols)
                  {  count[r][cx][cy]++;
                    if(count[r][cx][cy] > maxCount)
                      maxCount = count[r][cx][cy];
                  }
                }

      for(int r = 0 ;r < rsize; r++)
        for(int i = 0; i < src.rows; i++)
          for(int j = 0; j < src.cols; j++)
          {
              Vec3f circle;
              int threshold = maxCount*threshratio;
              if(count[r][i][j] > threshold)
                {
                  //cout << count[r][i][j] << endl;
                  circle[0] = r+rmin;
                  circle[1] = i;
                  circle[2] = j;
                  circles.push_back(circle);
                }

          }

        for( size_t i = 0; i < circles.size(); i++)
          for( size_t j = i+1; j < circles.size(); j++)
          {
          //  int cx, cy ,r;
            if(sqrt(pow(circles[i][1]-circles[j][1],2)+pow(circles[i][2]-circles[j][2],2)) < diff && abs(circles[i][0] - circles[j][0]) < diff)
            {
              circles[i][1] = (circles[i][1]+circles[j][1])/2;
              circles[i][2] = (circles[i][2]+circles[j][2])/2;
              circles[i][0] = (circles[i][0]+circles[j][0])/2;
              circles.erase(circles.begin()+j,circles.begin()+j+1);
              j--;
            }


          }


    return;

}

/** @function main */
int main(int argc, char** argv)
{
  Mat src, src_gray;

  /// Read the image
  src = imread( argv[1], 1 );

  if( !src.data )
    { return -1; }

  /// Convert it to gray
  cvtColor( src, src_gray, CV_BGR2GRAY );

  Mat edge;

  /// Reduce the noise so we avoid false circle detection
  GaussianBlur( src_gray, src_gray, Size(9, 9), 2, 2 );

  Canny(src_gray,edge, 120, 200, 3);


  vector<Vec3f> circles;

  CirHoughTrans(edge,circles,0.7,20,100,10);
  //
  // vector<Vec3f> circles;
  //
  // /// Apply the Hough Transform to find the circles
  // HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, 200, 100, 0, 0 );

  // Draw the circles detected
  cout << circles.size() << endl;
  for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][2]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][0]);
      //cout << center << " " << radius << endl;
      // circle center
      circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
      // circle outline
      circle( src, center, radius, Scalar(0,0,255), 3, 8, 0 );
   }

  /// Show your results
  namedWindow( "Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE );
  imshow( "Hough Circle Transform", edge);
  imshow( "Hough Circle Transform Demo", src);


  waitKey(0);
  return 0;
}

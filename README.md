Description:
-----------------------
It is second assignment for Computer vision course it is all about hough transformation(linear, circular and generalized) to detect Banknotes and coins 

Notice:
-------------
For Assignment Specifications and Requirements, Please Open Assginment02.pdf.
For the Results and Report, Open the PDF of the Report2.


Screenshots-
---------------------------------

![Alt text](https://bytebucket.org/noorio789/houghtransformation/raw/82abcd9cc050f9920a54586cb1545157f62da4dd/OnePaperTwoCoins.Noise.Screenshot.png "OnePaperTwoCoins.Noise.Screenshot")

![Alt text](https://bytebucket.org/noorio789/houghtransformation/raw/82abcd9cc050f9920a54586cb1545157f62da4dd/OnePaperTwoCoins.Plain.Screenshot.png " OnePaperTwoCoins.Plain.Screenshot")

![Alt text](https://bytebucket.org/noorio789/houghtransformation/raw/82abcd9cc050f9920a54586cb1545157f62da4dd/TwoPapersFiveCoins.Noisy.Screenshot.png "TwoPapersFiveCoins.Noisy.Screenshot")

![Alt text](https://bytebucket.org/noorio789/houghtransformation/raw/82abcd9cc050f9920a54586cb1545157f62da4dd/plain2.png "TwoPapersFiveCoins.Plain.Screenshot")

